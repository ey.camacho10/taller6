Feature: Finding Rutas

  Scenario: As a user I want to be able to open the help screen from the side menu the first time I open the app
    Given I press "Paraderos"               
    #button to remove the splash screen
    When I swipe left
    #to open te menu
    And I press "Ayuda"
    Then I should see "Calcular ruta con horario"

  Scenario: 1 - As a user I want to be able to open the help screen from the side menu the first time I open the app
    Given I press "Rutas"
    When I press "Portal El Dorado >> Universidades"
    Then I should see "Recorrido"
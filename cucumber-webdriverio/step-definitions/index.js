var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 2000);
    browser.click('button=Ingresar');
  });


  When(/^I fill with (.*), (.*), (.*), (.*), (.*) and (.*)$/ , (name, lastname, email, password, department, accept) => {
    var cajaSignUp = browser.element('.cajaSignUp');

    var nombreInput = cajaSignUp.element('input[name="nombre"]');
	nombreInput.click();
	nombreInput.clearElement();
	nombreInput.keys(name);
	
	var apellidoInput = cajaSignUp.element('input[name="apellido"]');
	apellidoInput.click();
	apellidoInput.clearElement();
	apellidoInput.keys(lastname);
	
	var correoInput = cajaSignUp.element('input[name="correo"]');
	correoInput.click();	
	correoInput.clearElement();
	correoInput.keys(email);
	
	var passwordInput = cajaSignUp.element('input[name="password"]');
	passwordInput.click();
	passwordInput.clearElement();
	passwordInput.keys(password);
	
	var selectInput = cajaSignUp.element('select[name="idDepartamento"]');
	selectInput.selectByValue("5");
	
	if (accept && accept == 'true'){
		var aceptaInput = cajaSignUp.element('input[name="acepta"]');
		if (!aceptaInput.isSelected()){
			aceptaInput.click();
		}
	}
  });
  
  When('I try to signup', () => {
    var cajaSignUp = browser.element('.cajaSignUp');
    cajaSignUp.element('button=Registrarse').click();
  });

  Then('I expect to see {string}', msj => {
	browser.waitForVisible('.sweet-alert', 3000);
	var alertText = browser.element('.sweet-alert').getText();
	expect(alertText).to.include(msj);
  });
  
  Then('I expect to get inputs error message {string}', msj => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
	var alertText = browser.element('.aviso.alert.alert-danger').getText();
	expect(alertText).to.include(msj);
  });
});
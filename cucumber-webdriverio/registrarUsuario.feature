Feature: Registrar usuario into losestudiantes
    As an user I want to create a user that I can enter on site

Scenario Outline: Registrar Usuario errores de campos

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill with <name>, <lastname>, <email>, <password>, <department> and <accept>
	And I try to signup
    Then I expect to get inputs error message <msj>

    Examples:
      | name      | lastname      | email                          | password | department | accept | msj                                             |
      | Edgar     | Camacho       | ey.camacho22@uniandes.edu.co   | 12345678 | 5          | false  | Debes aceptar los términos y condiciones        |
	  | Edgar     | Camacho       |                                | 12345678 | 5          | true   | Ingresa tu correo                               |
	  | Edgar     | Camacho       | ey.camacho22                   | 12345678 | 5          | true   | Ingresa un correo valido                        |
	  | Edgar     | Camacho       | ey.camacho22@uniandes.edu.co   |          | 5          | true   | Ingresa una contraseña                          |
	  | Edgar     | Camacho       | ey.camacho22@uniandes.edu.co   | 1234     | 5          | true   | La contraseña debe ser al menos de 8 caracteres |
 	  |           |               |                                |          |            |        | Ingresa tu correo                               |
	  
Scenario Outline: Registrar Usuario ya existe

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill with <name>, <lastname>, <email>, <password>, <department> and <accept>
    And I try to signup
    Then I expect to see <msj>

    Examples:
      | name      | lastname      | email                          | password | department | accept | msj                                            |
      | Edgar     | Camacho       | ey.camacho22@uniandes.edu.co   | 12345678 | 5          | true   | Ya existe un usuario registrado con el correo  |
	  
Scenario Outline: Registrar Usuario nuevo en la aplicacion

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill with <name>, <lastname>, <email>, <password>, <department> and <accept>
    And I try to signup
    Then I expect to see <msj>

    Examples:
      | name      | lastname      | email                          | password | department | accept | msj                    |
      | Edgar     | Camacho       | ey.camacho23@uniandes.edu.co   | 12345678 | 5          | true   | Registro exitoso       |